﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_ProminentFeatures_Demo
{
    class IntDatabase
    {
        public List<int> prices = new List<int>();

        public void Add(int price)
        {
            prices.Add(price);
        }

        public void Remove(int price)
        {
            prices.Remove(price);
        }

        public void Display()
        {
            Console.WriteLine("Price List"); //print only 1 item.
            foreach (var item in prices)
            {
                Console.WriteLine(item); //print all items from the list
            }
        }
    }
}
