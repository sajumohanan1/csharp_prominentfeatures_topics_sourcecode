﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharp_ProminentFeatures_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //change to Delegate


            /* Generics
             * Code reusability
             * int = integer
             * string = character
             * <T> = Generics
             */

            //Create a class

            //create object for the list
            //StringDatabase sdb = new StringDatabase();
            //sdb.Add("Television");
            //sdb.Add("Smart Phone");
            //sdb.Add("Refrigerator");
            //sdb.Display();

            ////Try to create a integer type of data.
            //// sdb.Add(10); -cannot pass the numerical arguments

            //IntDatabase idb = new IntDatabase();
            //idb.Add(10);
            //idb.Add(15);
            //idb.Add(20);
            //idb.Display();

            ////idb.Add("new item"); //generic to solve this issue

            ////Create object for Generic - specify the type of data
            //GenericDatabase<int> gidb = new GenericDatabase<int>();
            //gidb.Add(100);
            //gidb.Add(101);
            //gidb.Add(102);
            //gidb.Display();

            //GenericDatabase<string> gsdb = new GenericDatabase<string>();
            //gsdb.Add("Item 1");
            //gsdb.Add("Item 2");
            //gsdb.Add("Item 3");
            //gsdb.Display();

            //GenericDatabase<double> gddb = new GenericDatabase<double>();
            //gddb.Add(15.60);
            //gddb.Add(88.60);
            //gddb.Add(2.34);        
            //gddb.Display();

            ////Create object
            //Quad<string> myStringQuad = new Quad<string>();
            //myStringQuad.Item1 = "Laptop"; //assigned value for the array item called "Item1"
            //myStringQuad.Item2 = "Desktop"; //assigned value for the array item called "Item2"
            //myStringQuad.GetArray();

            //GenericPair<double, bool> intAndstring = new GenericPair<double, bool>();
            //intAndstring.Item1 = 666.90;
            //intAndstring.Item2 = true;
            //Console.WriteLine(intAndstring.Item1);
            //Console.WriteLine(intAndstring.Item2);


            //If the Id is null, then print 55
            //int tempid = intAndstring.Id ?? 0;
            //Console.WriteLine("ID :" + tempid);

            //Console.WriteLine(intAndstring.ItemName);

            //Delegates - with Lambda expression


            //Example for Delegate

            Customer c = new Customer()
            {
                Name = "Saju",
                Salary = 1700,
                LastThreeMonthsTransaction = 4800
            };


            //Create an object for the Delegate
            //CustomerDelegate cd = new CustomerDelegate(Approve);
            //Customer.IsEligibleForLoan(c, cd);

            //use lambda expression along with the delegate
            Customer.IsEligibleForLoan(c, customer=>
            customer.Salary > 1000 && customer.LastThreeMonthsTransaction > 3000);

            //Console.WriteLine(LambdaForCalculation.SumNum(15,5));
            
            //Lambda expressions are anonymous methods
            Func<int, int, int> ExSum = (x, y) => x + y;
            Console.WriteLine(ExSum(5, 2));


            Func<int, int, int> StmtSum = (x, y) =>
            {
                int total=x + y;
                Console.WriteLine($"Sum of {x} and {y} is {total}");
                return total;
            };
            Console.WriteLine(StmtSum(34,75));

            //LINQ - SQL commands
            //(1) Create a data source
            int[] numbers = new int[] { 16, 262, 67, 233 }; //Array is our data source

            //(2) create a Query - LINQ
            //Result=From element in Collection Where Condition Arrange Select element
            IEnumerable<int> result =
                from number in numbers
                where number > 100 //optional
                orderby number descending //optional
                select number;

            //(3) Execute the Query
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }



        }
        //public static bool Approve(Customer customer)
        //{
        //    if (customer.Salary > 1000 && customer.LastThreeMonthsTransaction > 3000)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
    }
}
