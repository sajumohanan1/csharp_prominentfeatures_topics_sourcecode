﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_ProminentFeatures_Demo
{
    class GenericPair<T,U>
    {
        public T Item1 { get; set; }
        public U Item2 { get; set; }

        //Null conditional operator
        public int? Id { get; set; } = null; //This can be null
        //There is a chance of getting null values from the external databases

        public string ItemName { get; set; } = "Smart Phone";
    }
}
