﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_ProminentFeatures_Demo
{
    class LambdaForCalculation
    {
        //normal method without delegate and lambda
        //internal static int SumNum(int x, int y)
        //{
        //    return x + y;
        //}

        //(1) Expression Lambda
        //Func = delegated used for lambda
        Func<int, int, int> ExSum=(x,y)=> x + y;
        


        //Statement Lambda
    }
}
