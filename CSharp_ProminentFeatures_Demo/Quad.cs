﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_ProminentFeatures_Demo
{
    class Quad<T>
    {
        //two generic properties are created
        public T Item1 { get; set; }
        public T Item2 { get; set; }



        public T[] GetArray()
        {
            //Generic type of array
            T[] quadArray = new T[] { Item1, Item2 }; //array property

            //print from array
            foreach (var item in quadArray)
            {
                Console.WriteLine(item);
            }
            return quadArray;

        }//method
    }//class
}//namespace
