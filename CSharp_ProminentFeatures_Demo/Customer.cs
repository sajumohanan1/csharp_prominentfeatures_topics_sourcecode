﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_ProminentFeatures_Demo
{
    //Create a Delegate
    public delegate bool CustomerDelegate(Customer customer);
    public class Customer
    {
        //Banking Application - to check whether eligible for a loan or not.
        //Create variables (fields)
        public string Name;
        public int Salary;
        public int LastThreeMonthsTransaction;

        //conditional check
        public static void IsEligibleForLoan(Customer c, CustomerDelegate cd)
        {
            if (cd(c))
            {
                Console.WriteLine("Loan is approved");
                return;
            }
            Console.WriteLine("Loan is not approved");
        }

    }
}
