﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_ProminentFeatures_Demo
{
    class StringDatabase
    {
        //String type of data
        //Create a list (without a Generic)
        public List<string> items = new List<string>();

        public void Add(string item)
        {
            items.Add(item);
        }

        public void Remove(string item)
        {
            items.Remove(item);
        }

        public void Display()
        {
            Console.WriteLine("Item List"); //print only 1 item.
            foreach (var item in items)
            {
                Console.WriteLine(item); //print all items from the list
            }
        }

    }
}
