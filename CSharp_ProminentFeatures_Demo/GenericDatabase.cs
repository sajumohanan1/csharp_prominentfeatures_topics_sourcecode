﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_ProminentFeatures_Demo
{
    class GenericDatabase<T> //Placeholder for the generic class
    {
        //Create a Generic List
        //T = any type of data // <T> - is  a placeholder
        public List<T> items = new List<T>();

        public void Add(T item)
        {
            items.Add(item);
        }

        public void Remove(T item)
        {
            items.Remove(item);
        }

        public void Display()
        {
            Console.WriteLine("List of " + typeof(T).Name + "'s"); //print only 1 item.
            foreach (var item in items)
            {
                Console.WriteLine(item); //print all items from the list
               
            }

            // items.ForEach(x => { Console.WriteLine(x); }); //single logic

        }
    }
}
